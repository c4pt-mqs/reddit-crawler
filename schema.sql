CREATE TABLE IF NOT EXISTS posts (
    id TEXT PRIMARY KEY,
    title TEXT,
    url TEXT,
    author TEXT,
    timestamp DATETIME,
    upvotes INTEGER,
    comments INTEGER,
    data TEXT
);