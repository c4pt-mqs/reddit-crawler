import sqlite3
import json

# Database connection
db = None

# Get a cursor from the database connection
def get_cursor():
    global db
    if db is None:
        db = sqlite3.connect('./reddit_posts.db')
    return db.cursor()

# Initialize the database
def initialize_database():
    c = get_cursor()
    c.execute("CREATE TABLE IF NOT EXISTS posts (id TEXT PRIMARY KEY, title TEXT, url TEXT, author TEXT, timestamp REAL, upvotes INTEGER, comments INTEGER, data TEXT)")
    c.close()
    db.commit()

# Save post to the database
def save_post(post):
    try:
        c = get_cursor()
        c.execute("SELECT id FROM posts WHERE id=?", (post.id,))
        existing_post = c.fetchone()
        if existing_post is None:
            post_data = {
                'id': post.id,
                'title': post.title,
                'url': post.url,
                'author': post.author.name,
                'timestamp': post.created_utc,
                'upvotes': post.score,
                'comments': post.num_comments
            }
            json_data = json.dumps(post_data)
            c.execute("INSERT INTO posts (id, title, url, author, timestamp, upvotes, comments, data)"
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                    (post.id, post.title, post.url, post.author.name, post.created_utc,
                    post.score, post.num_comments, json_data))
            db.commit()
        else:
            print(f"Post with id '{post.id}' already exists in the database.")
        c.close()
    except Exception as e:
        print(f"Error occurred during database operation: {str(e)}")
        c.close()
        db.rollback()
