import sqlite3
import pytest

@pytest.fixture
def connection():
    conn = sqlite3.connect(':memory:')
    cursor = conn.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS posts (
            id TEXT PRIMARY KEY,
            title TEXT,
            url TEXT,
            data TEXT
        )
    """)
    conn.commit()
    yield conn
    conn.close()

def test_database_schema(connection):
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()
    table_names = [table[0] for table in tables]
    assert 'posts' in table_names

def test_save_post(connection):
    cursor = connection.cursor()

    post = {
        'id': '123',
        'title': 'Test Post',
        'url': 'https://example.com',
        'data': '{"key": "value"}'
    }

    # Save the post
    cursor.execute("INSERT INTO posts (id, title, url, data) VALUES (?, ?, ?, ?)",
                   (post['id'], post['title'], post['url'], post['data']))
    connection.commit()

    # Retrieve the saved post
    cursor.execute("SELECT * FROM posts WHERE id=?", (post['id'],))
    saved_post = cursor.fetchone()
    assert saved_post is not None
    assert saved_post[0] == post['id']
    assert saved_post[1] == post['title']
    assert saved_post[2] == post['url']
    assert saved_post[3] == post['data']

    cursor.close()