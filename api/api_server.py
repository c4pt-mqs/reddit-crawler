from flask import Flask, jsonify, request
import logging
from logging.handlers import RotatingFileHandler
import os
import sys

# Add the parent directory of mypackage to the Python path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Import subpackage.submodule
from database.database_handler import get_cursor

app = Flask(__name__)

# Set up logging
logging.basicConfig(level=logging.INFO)

# Create a file handler
file_handler = RotatingFileHandler('reddit_crawler.log', maxBytes=1000000, backupCount=5)
file_handler.setLevel(logging.INFO)

# Create a stream handler
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

# Create a logging formatter
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# Set the formatter for both handlers
file_handler.setFormatter(formatter)

# Add the handlers to the logger
logger = logging.getLogger('')
logger.addHandler(file_handler)

# API endpoint to get posts with filters and sorting
@app.route('/posts', methods=['GET'])
def get_posts():
    with app.app_context():
        try:
            # Get a cursor from the database handler
            c = get_cursor()

            # Get query parameters
            keyword = request.args.get('keyword')
            start_date = request.args.get('start_date')
            end_date = request.args.get('end_date')
            min_upvotes = request.args.get('min_upvotes')
            max_upvotes = request.args.get('max_upvotes')

            # Build the SQL query
            query = "SELECT * FROM posts WHERE 1=1"
            params = []

            if keyword:
                query += " AND title LIKE ?"
                params.append(f"%{keyword}%")

            if start_date:
                query += " AND timestamp >= ?"
                params.append(start_date)

            if end_date:
                query += " AND timestamp <= ?"
                params.append(end_date)

            if min_upvotes:
                query += " AND upvotes >= ?"
                params.append(min_upvotes)

            if max_upvotes:
                query += " AND upvotes <= ?"
                params.append(max_upvotes)

            # Execute the query with parameters
            c.execute(query, params)
            all_posts = c.fetchall()

            # Convert rows to dictionary format
            posts = []
            for row in all_posts:
                post = {
                    'id': row[0],
                    'title': row[1],
                    'url': row[2],
                    'author': row[3],
                    'timestamp': row[4],
                    'upvotes': row[5],
                    'comments': row[6]
                }
                posts.append(post)

            c.close()
            return jsonify(posts)
        except Exception as e:
            logger.error(f"Error occurred during database operation: {str(e)}")
            return jsonify([]), 500

# Run the API server
if __name__ == '__main__':
    app.run(port=5001, debug=True)
