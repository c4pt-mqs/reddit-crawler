# Development stage
FROM python:3.9 AS development

WORKDIR /

COPY requirements.txt .
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

# Production stage
FROM python:3.9-slim AS production

WORKDIR /

COPY --from=development . .

CMD ["python3", "/main.py"]
