import praw
from database.database_handler import save_post

# Reddit credentials
CLIENT_ID = "YOUR_CLIENT_ID"
CLIENT_SECRET = "YOUR_CLIENT_SECRET"
USER_AGENT = "YOUR_USER_AGENT"

# Subreddit name
SUBREDDIT_NAME = "all"

# Login & Start crawling posts
def start_crawling():
    reddit = praw.Reddit(
        client_id=CLIENT_ID,
        client_secret=CLIENT_SECRET,
        user_agent=USER_AGENT
    )

    # Subreddit to crawl
    subreddit = reddit.subreddit(SUBREDDIT_NAME)

    # Continuously crawl new posts
    try:
        for post in subreddit.stream.submissions():
            save_post(post)
    except Exception as e:
        print(f"Error occurred during crawling: {str(e)}")
