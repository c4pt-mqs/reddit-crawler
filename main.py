#!/usr/bin/env python3

from crawler.reddit_crawler import start_crawling
from database.database_handler import initialize_database

# Initialize the database
initialize_database()

# Start crawling posts
start_crawling()
